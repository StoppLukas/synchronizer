# Synchronizer

* It is able to synchronize two of your Filesystem Directoriers
* It only copy missing files

### How do I get set up?

* Check it out on your local Machine
* Use eclipse to create a new Project
* Run the SynchronizerMain.java as an Java-Aplication

### Commands

* load: loads an Directory create hashes from the files.
* synk: synchronize the Files but dont copy them.
* copy: copies the missing files into the Directories who are missing them