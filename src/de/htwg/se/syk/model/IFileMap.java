package de.htwg.se.syk.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.NavigableSet;


public interface IFileMap {
    void    insert(String key, IMyFile file);
    void    insert(String key, ArrayList<IMyFile> fileList);
    void    addMissingFilesFrom(IFileMap fm);
    boolean contains(String key);
    
    String  getMyDir();
    void    setMyDir(String myDir);
    
    ArrayList<IMyFile> remove(String key)throws IOException;
    
    NavigableSet<String> getListOfTrackedKeys();
    ArrayList<IMyFile> getFilelistFromTrackedKey(String key);

    NavigableSet<String> getListOfMissingKeys();
    ArrayList<IMyFile> getFilelistFromMissingKey(String key);
}
