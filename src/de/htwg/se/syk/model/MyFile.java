package de.htwg.se.syk.model;


public class MyFile implements IMyFile {
	private long doc;
	private String name;
	private String absPath;
	private String relPath;
	private String hash;

	@Override
	public int hashCode(){
//	    Integer.parseInt(hash, 16);
	    return (int) Long.parseLong(hash, 16);
	}
	
	@Override
	public boolean equals(Object other) {
		if(other == null) return false;
		if(other == this) return true;
		if(!(other instanceof MyFile)) return false;
		
		MyFile otherFile = (MyFile)other;
		if(otherFile.getHash().equals(this.hash)) return true;
		
		return false;
	}
	
//Getter-Setter##############################################################################################################
	
	@Override
    public String getHash() {
	    return hash;
	}
	
	@Override
    public void setHash(String hash) {
	    this.hash = hash;
	}
	
	@Override
    public String getAbsPath() {
	    return absPath;
	}
	
	@Override
    public void setAbsPath(String absPath) {
	    this.absPath = absPath;
	}
	
	@Override
    public String getName() {
	    return name;
	}
	
	@Override
    public void setName(String name) {
	    this.name = name;
	}
	
	@Override
    public long getDoc() {
	    return doc;
	}
	
	@Override
    public void setDoc(long doc) {
	    this.doc = doc;
	}

    @Override
    public String getRelPath() {
        return relPath;
    }

    @Override
    public void setRelPath(String path) {
        this.relPath = path;
    }
}
