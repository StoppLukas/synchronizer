package de.htwg.se.syk.model;

import java.io.IOException;

public interface IFileFactory {

    public abstract MyFile create() throws IOException;

}