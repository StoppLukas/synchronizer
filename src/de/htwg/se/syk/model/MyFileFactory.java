package de.htwg.se.syk.model;

import java.io.IOException;

public class MyFileFactory implements IFileFactory {
    @Override
    public MyFile create() throws IOException {
        return new MyFile();
    }
}
