package de.htwg.se.syk.model;

import java.util.ArrayList;

public class SharedReadFilemapArray {
    private static ArrayList<IFileMap> readFilemapArray = new ArrayList<IFileMap>();
    
    public static synchronized void addFilemap(IFileMap fileMap){
        readFilemapArray.add(fileMap);
    }
    
    public static synchronized void removeFileMap(String rootPath){
        for (IFileMap iFileMap : readFilemapArray) {
            if(iFileMap.getMyDir().equals(rootPath)){
                readFilemapArray.remove(readFilemapArray.indexOf(iFileMap));
            }
        }
    }
    
    public static synchronized void removeAll(){
        readFilemapArray.clear();
    }
    
    public static synchronized ArrayList<IFileMap> getReadFilemapArray() {
        return readFilemapArray;
    }

    public static void removeFileMap(IFileMap fileMap) {
        readFilemapArray.remove(fileMap);
    }
}
