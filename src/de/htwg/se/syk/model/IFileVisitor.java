package de.htwg.se.syk.model;

import java.io.File;

public interface IFileVisitor {
    public void visit(File file);
}
