package de.htwg.se.syk.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.NavigableSet;
import java.util.TreeMap;

public class FileMap implements IFileMap {
	private TreeMap<String, ArrayList<IMyFile>> myTrackedFiles = new TreeMap<String, ArrayList<IMyFile>>();
	private TreeMap<String, ArrayList<IMyFile>> myMissingFiles = new TreeMap<String, ArrayList<IMyFile>>();
    private String myDir;
	
    public FileMap() {
    }
    
	@Override
	public void addMissingFilesFrom(IFileMap fileMap){
	    for (String current : fileMap.getListOfTrackedKeys()) {
	        if(myTrackedFiles.containsKey(current)){
	            continue;
	        }
	        
	        myMissingFiles.put(current, fileMap.getFilelistFromTrackedKey(current));
        }
	}
	
	@Override
    public ArrayList<IMyFile> getFilelistFromTrackedKey(String key){
	    return myTrackedFiles.get(key);
	}
	
	@Override
	public NavigableSet<String> getListOfTrackedKeys() {
	    return myTrackedFiles.descendingKeySet();
	}

	@Override
	public NavigableSet<String> getListOfMissingKeys() {
	    return myMissingFiles.descendingKeySet();
	}
	
	@Override
	public ArrayList<IMyFile> getFilelistFromMissingKey(String key) {
	    return myMissingFiles.get(key);
	}

	@Override
    public void insert(String key, IMyFile file){
	    if(myTrackedFiles.containsKey(key)){
	        myTrackedFiles.get(key).add(file);
	    } else {
	        ArrayList<IMyFile> al = new ArrayList<IMyFile>();
	        al.add(file);
	        myTrackedFiles.put(key, al);
	    }
	}

	@Override
	public void insert(String key, ArrayList<IMyFile> fileList) {
	    if(myTrackedFiles.containsKey(key)){
	        myTrackedFiles.get(key).addAll(fileList);
	    } else {
	        myTrackedFiles.put(key, fileList);
	    }
	}
	
	@Override
    public ArrayList<IMyFile> remove(String key) throws IOException{
	    if(myTrackedFiles.containsKey(key)){
	        ArrayList<IMyFile> al = myTrackedFiles.get(key);
	        myTrackedFiles.remove(key);
	        return al;
	    }
	    return null;
	}

    @Override
    public boolean contains(String key) {
        return myTrackedFiles.containsKey(key);
    }

    @Override
    public String getMyDir() {
        return myDir;
    }

    @Override
    public void setMyDir(String myDir) {
        this.myDir = myDir;
    }
    
    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof FileMap)){
            return false;
        }
        FileMap fm = (FileMap) obj;
        return fm.getMyDir().equals(this.getMyDir());
    }
    
    @Override
    public int hashCode() {
        return myDir.hashCode();
    }


}
