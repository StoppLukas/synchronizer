package de.htwg.se.syk.model;

public interface IMyFile {
    String  getHash();
    void    setHash(String hash);
    
    String  getAbsPath();
    void    setAbsPath(String absPath);

    String  getName();
    void    setName(String name);
    
    long    getDoc();
    void    setDoc(long doc);
    
    String  getRelPath();
    void    setRelPath(String path);
}
