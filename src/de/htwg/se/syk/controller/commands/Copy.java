package de.htwg.se.syk.controller.commands;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.NavigableSet;
import org.apache.commons.io.FileUtils;
import de.htwg.se.syk.model.IFileMap;
import de.htwg.se.syk.model.IMyFile;
import de.htwg.se.syk.model.SharedReadFilemapArray;
import de.htwg.se.syk.utility.IObservable;

public class Copy implements ICommand {

    /**
     * The "copy" method, will copy all the missing files with there folders into the folder who is missing them.
     * If the "synk" method is not called at this moment, the "copy" method will also copy the files, but there are none...
     * Moreover, it will clear the shared Arrays and maps.
     * @throws IOException
     * @param param: this will never be used! 
     */
    public void work(String param, IObservable observable) throws IOException{
        ArrayList<IFileMap> readFilemapArray = SharedReadFilemapArray.getReadFilemapArray();
        
        for (IFileMap fileMap : readFilemapArray) {
            String destRootPath = fileMap.getMyDir();
            
            NavigableSet<String> missingFileKeys = fileMap.getListOfMissingKeys();
            
            for (String key : missingFileKeys) {
                ArrayList<IMyFile> missingFilesForCurrentHashkey = fileMap.getFilelistFromMissingKey(key);
                copyArrayToDestination(missingFilesForCurrentHashkey, destRootPath, observable);
            }
        }
        SharedReadFilemapArray.removeAll();
    }
    
    private void copyArrayToDestination(ArrayList<IMyFile> arr, String destRootPath, IObservable observable) throws IOException{
        for (IMyFile myFile : arr) {
            File srcFile = new File(myFile.getAbsPath());
            File dstFile = new File(destRootPath + "/" + myFile.getRelPath());
            
            observable.informCopyFile(myFile.getRelPath(), destRootPath);
            FileUtils.copyFile(srcFile, dstFile);
        }
    }
}
