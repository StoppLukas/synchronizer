package de.htwg.se.syk.controller.commands;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import org.apache.commons.codec.digest.DigestUtils;
import de.htwg.se.syk.model.IFileMap;
import de.htwg.se.syk.model.IFileVisitor;
import de.htwg.se.syk.model.IMyFile;
import de.htwg.se.syk.model.SharedReadFilemapArray;
import de.htwg.se.syk.utility.DI;
import de.htwg.se.syk.utility.IObservable;


public class Load implements ICommand {
    
    /**
     * Read all files in the Given folder, with the internal visitor.
     * @param path
     * @throws NoSuchAlgorithmException
     * @throws IOException
     */
    public void work(final String path, IObservable observable) throws NoSuchAlgorithmException, IOException {
        File folder = new File(path);
        final IFileMap fileMap = DI.getContext().getBean(IFileMap.class);
        fileMap.setMyDir(folder.getAbsolutePath());
        read(folder, new AddToFileMapVisitor(fileMap));
        SharedReadFilemapArray.addFilemap(fileMap);
        observable.informLoadFinished(fileMap);
    }
    
    /**
     * Read all files in the given folder, with the given visitor. only private, until it works right!
     * @param folder
     * @param visitor
     */
    private void read(final File folder, final IFileVisitor visitor) {
        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {
                read(fileEntry, visitor);
            } else {
                visitor.visit(fileEntry);
            }
        }
    }

    private void setFileContent(IMyFile mf, final File fe, String srcPath) throws FileNotFoundException, IOException {
        mf.setAbsPath(fe.getAbsolutePath());
        mf.setDoc(fe.lastModified());
        mf.setName(fe.getName());
        
        FileInputStream fi = new FileInputStream(fe);
        mf.setHash(DigestUtils.md5Hex(fi));
        fi.close();
        
        mf.setRelPath(calcRelativePath(srcPath, fe.getAbsolutePath()));
    }
    
    private String calcRelativePath(String srcFolderPath, String absFilePath){
        absFilePath = absFilePath.replace("\\","/");
        srcFolderPath = srcFolderPath.replace("\\","/");
        return absFilePath.split(srcFolderPath)[1];
    }

    private final class AddToFileMapVisitor implements IFileVisitor {
        private final IFileMap fileMap;
        
        private AddToFileMapVisitor(IFileMap fileMap) {
            this.fileMap = fileMap;
        }
        
        @Override
        public void visit(File file) {
            try {
                IMyFile mf = DI.getContext().getBean(IMyFile.class);
                setFileContent(mf, file, fileMap.getMyDir());
                fileMap.insert(mf.getHash(), mf);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }
}
