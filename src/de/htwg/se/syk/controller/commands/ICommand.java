package de.htwg.se.syk.controller.commands;

import de.htwg.se.syk.utility.IObservable;

public interface ICommand {
    void work(String parameters, IObservable observable) throws Exception;
}
