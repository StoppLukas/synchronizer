package de.htwg.se.syk.controller.commands;

import java.util.ArrayList;
import java.util.Iterator;
import de.htwg.se.syk.model.FileMap;
import de.htwg.se.syk.model.IFileMap;
import de.htwg.se.syk.model.SharedReadFilemapArray;
import de.htwg.se.syk.utility.IObservable;

public class Synk implements ICommand{
    
    /**
     * This Method will "synk" all the read files.
     * In every read FileMap is a list of missing files, this list is modified by "synk".
     * So "synk" makes only a diff, NOT a copy!!!
     */
    public void work(String param, IObservable observable){
        ArrayList<IFileMap> readFilemapArray = SharedReadFilemapArray.getReadFilemapArray();
        
        for (Iterator<IFileMap> iterator = readFilemapArray.iterator(); iterator.hasNext();) {
            FileMap fileMap = (FileMap) iterator.next();
            
            for (Iterator<IFileMap> iterator2 = readFilemapArray.iterator(); iterator2.hasNext();) {
                FileMap fileMap2 = (FileMap) iterator2.next();
                if(fileMap.equals(fileMap2)) {continue;}
                
                fileMap.addMissingFilesFrom(fileMap2);
            }
            observable.informSynkFinished(fileMap);
        }
    }
}
