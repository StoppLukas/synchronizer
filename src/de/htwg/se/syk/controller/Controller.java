package de.htwg.se.syk.controller;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.BlockingQueue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import de.htwg.se.syk.controller.commands.ICommand;
import de.htwg.se.syk.model.IFileMap;
import de.htwg.se.syk.utility.DI;
import de.htwg.se.syk.utility.IObservable;
import de.htwg.se.syk.utility.Observable;
import de.htwg.se.syk.utility.IObserver;

public class Controller extends Thread implements IObservable{
    /* Get actual class name to be printed on */
    static final Logger log = LogManager.getLogger();
    
    private static final int MAX_ARRAY_LENGTH = 2;
    private static final int MAX_SPLIT_COUNT = 2;
    private static final String QUIT = "q";
    
    private BlockingQueue<String> commandQu = new LinkedBlockingQueue<>();
    private IObservable observerManager = new Observable(){};
    ICommand command;
    
    
    /**
     * This Method blocks while waiting for new commands in the Command-Queue.
     * If there is an command and the command is "q", this Method run out.
     * Otherwise it will try to get an Instance of the command, and run the Method "work" of the command.
     * The Method will print an Error if the command wasn't valid, but don't break!
     */
    @Override
    public void run(){
        String in;
        while(true){
            try {
                in = commandQu.take();
            } catch (Exception e) {
                continue;
            }
            
            if(in.equals(QUIT)){
                break;
            }
            
            String[] input = in.split(" ", MAX_SPLIT_COUNT);
            
            try{
                command = (ICommand) DI.getContext().getBean(input[0]);
            } catch(NoSuchBeanDefinitionException e){
                log.error("not an valid Command, try it again...");
                continue;
            }
            
            try{
                if (input.length == MAX_ARRAY_LENGTH) {
                    command.work(input[1], this);
                } else {
                    command.work(null, this);
                }
            } catch(Exception e){
                log.error("Error happend on " + input[1] + ". I ignore them and go ahead.");
            }
        }
    }

    public synchronized void pushCommand(String command) {
        commandQu.add(command);
    }
    
    @Override
    public synchronized void addObserver(IObserver s) {
        observerManager.addObserver(s);
    }

    @Override
    public synchronized void removeObserver(IObserver s) {
        observerManager.removeObserver(s);
    }

    @Override
    public synchronized void removeAllObservers() {
        observerManager.removeAllObservers();
    }

    @Override
    public void informLoadFinished(IFileMap fileMap) {
        observerManager.informLoadFinished(fileMap);
    }

    @Override
    public void informSynkFinished(IFileMap fileMap) {
        observerManager.informSynkFinished(fileMap);
    }

    @Override
    public void informCopyFile(String src, String dst) {
        observerManager.informCopyFile(src, dst);
    }
}
