package de.htwg.se.syk.aVIEW.aTUI;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.NavigableSet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import de.htwg.se.syk.controller.Controller;
import de.htwg.se.syk.model.IFileMap;
import de.htwg.se.syk.utility.IObserver;

public class TUI extends Thread implements IObserver {
    /* Get actual class name to be printed on */
    static final Logger log = LogManager.getLogger();
    
    private Controller controller = null;
    private static final String QUIT = "q";
    private static final String HELP = "h";
    
    public void setController(Controller c){
        this.controller = c;
    }
    
    @Override
    public void run() {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        writeHeader();
        
        while(true){
            String in = "";
            try {
                in = br.readLine();
            } catch (IOException e) {
                log.debug(e);
                continue;
            }
            log.debug("Running command {}", in);
            if(in.equals(HELP)){
                writeHeader();
                continue;
            }
            
            controller.pushCommand(in);
            if(in.equals(QUIT)){
                System.exit(0);
            }
        }
    }

    private void writeHeader() {
        ol("");
        ol("#################################################################################");
        ol("# Your options:                                                                 #");
        ol("# 1. load <absolutPath> (Will load all files from <absolutPath>)                #");
        ol("# 2. synk               (Check whith files will be copied)                      #");
        ol("# 3. copy               (Will Copy all neaded files in every loaded Directory -)#");
        ol("#                       (-and clears all loaded ans synked Directories!)        #");
        ol("# 4. q                  (Will close the Programm)                               #");
        ol("# 5. h                  (For this help...)                                      #");
        ol("#################################################################################");
    }
    
    public void setMainController(Controller ct){
        this.controller = ct;
    }

    
    void ol (String s){
        log.info(s);
    }

    @Override
    public void updateLoadFinished(IFileMap fileMap) {
        NavigableSet<String> trackedKeys = fileMap.getListOfTrackedKeys();
        for (String hash : trackedKeys) {
            ol("loaded: " + fileMap.getFilelistFromTrackedKey(hash).get(0).getRelPath());
        }
    }

    @Override
    public void updateSynkFinished(IFileMap fileMap) {
        NavigableSet<String> missingKeys = fileMap.getListOfMissingKeys();
        for (String hash : missingKeys) {
            ol("Dir: "+ fileMap.getMyDir() + "\tis missing: " + fileMap.getFilelistFromMissingKey(hash).get(0).getRelPath());
        }
    }

    @Override
    public void updateCopyFile(String src, String dst) {
        ol("Copy: " + src + "\t\tin: " + dst);
    }

}
