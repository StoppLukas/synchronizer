package de.htwg.se.syk.aVIEW;

import de.htwg.se.syk.aVIEW.aGUI.GUI;
import de.htwg.se.syk.aVIEW.aTUI.TUI;
import de.htwg.se.syk.controller.Controller;
import de.htwg.se.syk.utility.DI;

public class SynchronizerMain{
	private static Controller controller;
	private static TUI tui;
	private static GUI gui;

	public static void main(String[] args){
		controller = (Controller) DI.getContext().getBean("Controller");
		
		tui = (TUI) DI.getContext().getBean("TUI");
		tui.setController(controller);
		
		gui = (GUI) DI.getContext().getBean("GUI");
		gui.setController(controller);
		gui.initialize();

		controller.addObserver(tui);
		controller.addObserver(gui);

		controller.start();
		tui.start();
	}
}
