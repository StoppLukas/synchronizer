package de.htwg.se.syk.aVIEW.aGUI;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import de.htwg.se.syk.model.IMyFile;

public class FileEntry extends JPanel{
	private static final long serialVersionUID = 1L;
	private JLabel fileLabel;

	public FileEntry(){
		setBorder(new CompoundBorder(new EmptyBorder(2, 2, 1, 1), new EtchedBorder(EtchedBorder.LOWERED, null, null)));
		
		fileLabel = new JLabel("New label");
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addComponent(fileLabel, GroupLayout.DEFAULT_SIZE, 265, Short.MAX_VALUE)
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addComponent(fileLabel, GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE)
		);
		setLayout(groupLayout);
	}

	public void updateInput(IMyFile iMyFile){
		fileLabel.setText(iMyFile.getRelPath());
		this.revalidate();
	}
	
	public void updateInput(String s){
	    fileLabel.setText(s);
	    this.revalidate();
	}
}
