package de.htwg.se.syk.aVIEW.aGUI;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import de.htwg.se.syk.controller.Controller;
import de.htwg.se.syk.model.IFileMap;
import de.htwg.se.syk.utility.IObserver;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import java.awt.BorderLayout;
import javax.swing.Box;
import java.awt.Dimension;

public class GUI implements IObserver {
    private JFrame frmSynchronizer;
    private Controller controller;
    private boolean initialized = false;
    private Box folders;
    private FileListPanel cpfileListPanel;
    
    public GUI() {}

    /**
     * @wbp.parser.entryPoint
     */
    public void initialize() {
        if (initialized) {
            throw new IllegalStateException("already initialized!");
        }
        this.initialized = true;
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {}

        frmSynchronizer = new JFrame();
        frmSynchronizer.setTitle("Synchronizer");
        frmSynchronizer.setBounds(100, 100, 868, 511);
        frmSynchronizer.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JMenuBar menuBar = new JMenuBar();
        frmSynchronizer.setJMenuBar(menuBar);

        JButton btnNewButton = new JButton("Add Folder to Selection");
        btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFileChooser fc = new JFileChooser();
                fc.setCurrentDirectory(new File("."));
                fc.setMultiSelectionEnabled(true);
                fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                int r1 = fc.showDialog(frmSynchronizer, "Choose Folder(s) to Synchronize");
                if (r1 == JFileChooser.APPROVE_OPTION) {
                    File[] selectedFiles = fc.getSelectedFiles();
                    for (File file : selectedFiles) {
                        controller.pushCommand("load " + file.getAbsolutePath());
                    }
                }
            }
        });
        menuBar.add(btnNewButton);

        JButton btnNewButton_1 = new JButton("Close Application");
        btnNewButton_1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        JButton btnSynchronize = new JButton("Synchronize");
        btnSynchronize.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	folders.removeAll();
                controller.pushCommand("synk");
            }
        });
        menuBar.add(btnSynchronize);
        
        JButton btnCopy = new JButton("Copy");
        btnCopy.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                folders.removeAll();
                cpfileListPanel = new FileListPanel(folders);
                cpfileListPanel.setMaximumSize(new Dimension(400, 32777));
                folders.add(cpfileListPanel);
                controller.pushCommand("copy");
            }
        });
        menuBar.add(btnCopy);
        menuBar.add(btnNewButton_1);

        JScrollPane scrollPane = new JScrollPane();
        frmSynchronizer.getContentPane().add(scrollPane, BorderLayout.CENTER);

        folders = Box.createHorizontalBox();
        scrollPane.setViewportView(folders);

        frmSynchronizer.setVisible(true);
    }

    public void setController(Controller controller) {
        this.controller = controller;
    }

    @Override
    public void updateLoadFinished(final IFileMap fileMap) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                FileListPanel fileListPanel = new FileListPanel(folders);
                fileListPanel.setMaximumSize(new Dimension(400, 32777));
                fileListPanel.updateInput(fileMap);
                folders.add(fileListPanel);
                folders.revalidate();
            }
        });
    }

    @Override
    public void updateSynkFinished(final IFileMap fileMap) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                
                FileListPanel fileListPanel = new FileListPanel(folders);
                fileListPanel.setMaximumSize(new Dimension(400, 32777));
                fileListPanel.updateSynkInput(fileMap);
                folders.add(fileListPanel);
                
                folders.revalidate();
                folders.repaint();
                
                
            }
        });
    }

    @Override
    public void updateCopyFile(final String src, final String dst) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                cpfileListPanel.updateCopyInput(src, dst);
                
                folders.revalidate();
                folders.repaint();
            }
        });
    }
}
