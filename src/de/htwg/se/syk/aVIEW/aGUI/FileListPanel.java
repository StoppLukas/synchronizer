package de.htwg.se.syk.aVIEW.aGUI;

import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JScrollPane;
import javax.swing.Box;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import de.htwg.se.syk.model.IFileMap;
import de.htwg.se.syk.model.IMyFile;
import de.htwg.se.syk.model.SharedReadFilemapArray;
import javax.swing.JButton;
import javax.swing.JComponent;
import java.awt.Component;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.NavigableSet;
import java.awt.FlowLayout;
import java.awt.Dimension;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FileListPanel extends JPanel {
    private static final long serialVersionUID = 1L;
    private JLabel folderLabel;
    private Box verticalBox;
    private IFileMap fileMap;
    private boolean oneTime = true;
    private JComponent fileListParent;

    private JButton btnNewButton = new JButton("remove folder from selection");
    
    /**
     * Create the panel.
     */
    public FileListPanel(final JComponent parent) {
        this.fileListParent = parent;
        setBorder(new CompoundBorder(new EmptyBorder(0, 0, 0, 0), new CompoundBorder(new EtchedBorder(EtchedBorder.RAISED, null, null), new EmptyBorder(3, 3, 3, 3))));

        JScrollPane scrollPane = new JScrollPane();

        JPanel panel = new JPanel();
        FlowLayout flowLayout = (FlowLayout) panel.getLayout();
        flowLayout.setVgap(0);
        flowLayout.setHgap(0);

        folderLabel = new JLabel("Folder: ");
        GroupLayout groupLayout = new GroupLayout(this);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addComponent(panel, GroupLayout.DEFAULT_SIZE, 378, Short.MAX_VALUE).addComponent(scrollPane, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 378, Short.MAX_VALUE).addComponent(folderLabel, GroupLayout.DEFAULT_SIZE, 378, Short.MAX_VALUE));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.TRAILING).addGroup(groupLayout.createSequentialGroup().addComponent(folderLabel).addPreferredGap(ComponentPlacement.RELATED).addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 245, Short.MAX_VALUE).addPreferredGap(ComponentPlacement.RELATED).addComponent(panel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)));

        verticalBox = Box.createVerticalBox();
        scrollPane.setViewportView(verticalBox);

        
        btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (fileMap != null)
                    SharedReadFilemapArray.removeFileMap(fileMap);
                verticalBox.removeAll();
                fileListParent.remove(FileListPanel.this);
                fileListParent.revalidate();
                fileListParent.repaint();
            }
        });
        btnNewButton.setMargin(new Insets(0, 14, 0, 14));
        btnNewButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(btnNewButton);
        setLayout(groupLayout);

    }

    public void updateInput(IFileMap fileMap) {
        folderLabel.setText("Folder: " + fileMap.getMyDir());
        this.fileMap = fileMap;
        NavigableSet<String> listOfTrackedKeys = fileMap.getListOfTrackedKeys();
        for (String string : listOfTrackedKeys) {
            ArrayList<IMyFile> filelistFromTrackedKey = fileMap.getFilelistFromTrackedKey(string);
            for (IMyFile iMyFile : filelistFromTrackedKey) {
                FileEntry fileEntry = new FileEntry();
                fileEntry.setMinimumSize(new Dimension(200, 18));
                fileEntry.setMaximumSize(new Dimension(32771, 50));
                fileEntry.updateInput(iMyFile);
                verticalBox.add(fileEntry);
                verticalBox.revalidate();
            }
        }
    }

    public void updateSynkInput(IFileMap fileMap) {
        btnNewButton.setEnabled(false);
        btnNewButton.setVisible(false);
        folderLabel.setText("Folder : " + fileMap.getMyDir() + " is Missing:");
        this.fileMap = fileMap;
        NavigableSet<String> listOfMissingKeys = fileMap.getListOfMissingKeys();
        for (String string : listOfMissingKeys) {
            ArrayList<IMyFile> filelistFromMissingKey = fileMap.getFilelistFromMissingKey(string);
            for (IMyFile iMyFile : filelistFromMissingKey) {
                FileEntry fileEntry = new FileEntry();
                fileEntry.setMinimumSize(new Dimension(200, 18));
                fileEntry.setMaximumSize(new Dimension(32771, 50));
                fileEntry.updateInput(iMyFile);
                verticalBox.add(fileEntry);
                verticalBox.revalidate();
            }
        }
    }

    public void updateCopyInput(String src, String dst) {
        if (oneTime) {
            folderLabel.setText("Copied files: source path -> destination path");
        }
        oneTime = false;

        FileEntry fileEntry = new FileEntry();
        fileEntry.setMinimumSize(new Dimension(200, 18));
        fileEntry.setMaximumSize(new Dimension(32771, 50));
        fileEntry.updateInput(src + "   ->   " + dst);
        verticalBox.add(fileEntry);
        verticalBox.revalidate();
    }
}
