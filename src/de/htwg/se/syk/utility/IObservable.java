package de.htwg.se.syk.utility;

import de.htwg.se.syk.model.IFileMap;

public interface IObservable {
	void addObserver(IObserver s);
    void removeObserver(IObserver s);
    void removeAllObservers();

    void informLoadFinished(IFileMap fileMap);
    void informSynkFinished(IFileMap fileMap);
    void informCopyFile(String src, String dst);
}
