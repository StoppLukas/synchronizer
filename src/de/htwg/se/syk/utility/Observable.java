package de.htwg.se.syk.utility;

import java.util.ArrayList;
import java.util.List;
import de.htwg.se.syk.model.IFileMap;

public abstract class Observable implements IObservable {
	
	private List<IObserver> subcribers = new ArrayList<IObserver>(1);

    @Override
    public void addObserver(IObserver s) {
        subcribers.add(s);
    }

    @Override
    public void removeObserver(IObserver s) {
        subcribers.remove(s);
    }

    @Override
    public void removeAllObservers() {
        subcribers.clear();
    }

    @Override
    public void informLoadFinished(IFileMap fileMap) {
        for (IObserver observer : subcribers) {
            observer.updateLoadFinished(fileMap);
        }
    }

    @Override
    public void informSynkFinished(IFileMap fileMap) {
        for (IObserver observer : subcribers) {
            observer.updateSynkFinished(fileMap);
        }
    }

    @Override
    public void informCopyFile(String src, String dst) {
        for (IObserver observer : subcribers) {
            observer.updateCopyFile(src, dst);
        }
    }
    
}
