package de.htwg.se.syk.utility;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public final class DI {
    private static ApplicationContext context = new ClassPathXmlApplicationContext("config.xml");
    
    public static ApplicationContext getContext() {
        return context;
    }
}
