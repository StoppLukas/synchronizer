package de.htwg.se.syk.utility;

import de.htwg.se.syk.model.IFileMap;

public interface IObserver {
	void updateLoadFinished(final IFileMap fileMap);
	void updateSynkFinished(final IFileMap fileMap);
	void updateCopyFile(String src, String dst);
}
