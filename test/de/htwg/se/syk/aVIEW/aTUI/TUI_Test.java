package de.htwg.se.syk.aVIEW.aTUI;

import java.io.File;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import de.htwg.se.syk.controller.Controller;
import de.htwg.se.syk.model.SharedReadFilemapArray;
import de.htwg.se.syk.utility.DI;

@Ignore
public class TUI_Test {
    private static final String workDir = System.getProperty("user.dir").replace("\\", "/");
    private static final String tf1 = "/tf_1";
    
    private TUI tui = null;
    private Controller controller = null;
    
    @Before
    public void before() {
        SharedReadFilemapArray.removeAll();
    }
    
    @Test
    public void test() throws Exception {
        
        controller = (Controller) DI.getContext().getBean("Controller");
        tui = (TUI) DI.getContext().getBean("TUI");
        tui.setController(controller);
        
        controller.addObserver(tui);
        
        controller.start();
        tui.start();
        
        //add some commands
        controller.pushCommand("load" + workDir + tf1);
        controller.pushCommand("nix");
        controller.pushCommand("q");
        
        
        tui.join();
        controller.join();
    }
    
    @After
    public void after() {
        SharedReadFilemapArray.removeAll();
        File f = new File(workDir + tf1);
        removeAllFilesFromTestfolder(f);
    }
    
    private void removeAllFilesFromTestfolder(File folder) {
        
        for (File file : folder.listFiles()) {
            if (!file.getName().equals("1.jpg")){
                FileUtils.deleteQuietly(file);
            }
        }
    }

}
