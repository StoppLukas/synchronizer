package de.htwg.se.syk.modle;

import static org.junit.Assert.*;
import java.io.IOException;
import java.lang.reflect.Array;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.NavigableSet;
import org.junit.Test;
import de.htwg.se.syk.model.FileMap;
import de.htwg.se.syk.model.IFileMap;
import de.htwg.se.syk.model.IMyFile;
import de.htwg.se.syk.model.MyFile;
import de.htwg.se.syk.model.MyFileFactory;
import de.htwg.se.syk.utility.DI;

public class FileMap_Test {
	
	private String workDir = System.getProperty("user.dir").replace("\\", "/");
	private String tf1 = "/tf_1";
	private String tf2 = "/tf_2";

	@Test
	public void FM_test() throws NoSuchAlgorithmException, IOException {
	    IFileMap fm = DI.getContext().getBean(IFileMap.class);
	    IFileMap fm2 = DI.getContext().getBean(IFileMap.class);
	    
	    IMyFile mf1 = DI.getContext().getBean(IMyFile.class);
	    IMyFile mf2 = DI.getContext().getBean(IMyFile.class);
	    IMyFile mf3 = DI.getContext().getBean(IMyFile.class);
	    IMyFile mf4 = DI.getContext().getBean(IMyFile.class);
	    
	    mf1.setAbsPath("asdf");
	    mf1.setHash("asdf");
	    mf1.setName("asdf");
	    mf1.setRelPath("asdf");

	    mf2.setAbsPath("qwer");
	    mf2.setHash("qwer");
	    mf2.setName("qwer");
	    mf2.setRelPath("qwer");
	    
	    mf4.setAbsPath("yxcv");
	    mf4.setHash("yxcv");

	    ArrayList<IMyFile> alF= new ArrayList<>();
	    alF.add(mf2);
	    alF.add(mf3);
	    
		fm.insert("asdf", mf1);
		fm.insert("qwer", mf2);
		fm.insert("asdf", mf3);
		fm.insert("nix", alF);
		fm.insert("nix", alF);
		
		fm2.insert("yxcv", mf4);
		fm2.insert("asdf", mf1);
		fm2.setMyDir("garnix");
		
		NavigableSet<String> trackedKeys = fm.getListOfTrackedKeys();
		String key = trackedKeys.first();

		assertTrue(fm.contains(key));
		assertNotNull(fm.getFilelistFromTrackedKey(key));
		assertNull(fm.remove("gibtsNich"));
		assertNotNull(fm.remove(key));
		
		fm.addMissingFilesFrom(fm2);
		
		NavigableSet<String> missingKeys = fm.getListOfMissingKeys();
		key = missingKeys.first();
		
		assertNotNull(fm.getFilelistFromMissingKey(key));
		fm2.hashCode();
		
		
//		NavigableSet<String> missingKeys = fm.getListOfMissingKeys();
//		IMyFile t = fm.getFilelistFromTrackedKey(trackedKeys.first()).get(0);
//		IMyFile m = fm.getFilelistFromMissingKey(missingKeys.first()).get(0);
		
		
	}
	
	@Test
	public void equals(){
	    FileMap fm = new FileMap();
	    fm.setMyDir("asdf");
	    
	    FileMap fa = new FileMap();
	    fa.setMyDir("qwer");
	    
	    FileMap fp = new FileMap();
	    fp.setMyDir("asdf");
	    
	    MyFile mf = new MyFile();
	    
	    assertNotEquals(fm, mf);
	    assertNotEquals(fm, fa);
	    assertEquals(fm, fm);
	    assertEquals(fm, fp);
	    
	}

}
