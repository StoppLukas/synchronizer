package de.htwg.se.syk.modle;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Test;
import de.htwg.se.syk.model.IFileFactory;
import de.htwg.se.syk.model.MyFile;
import de.htwg.se.syk.utility.DI;

public class MyFile_Test {
	
	private String workDir = System.getProperty("user.dir").replace("\\", "/");
	private String tf2 = "/tf_2";

	@Test
	public void MF_test() throws NoSuchAlgorithmException, IOException {
		MyFile mf = new MyFile();
		assertNotNull(mf);
	}
	
	@Test
	public void equals_null_test() throws NoSuchAlgorithmException, IOException {
	    File f = new File(workDir + tf2 + "/1.jpg");
		MyFile mf1 = new MyFile();
		read(mf1, f);
		MyFile mfn = null;
		assertFalse(mf1.equals(mfn));
	}
	
	@Test
	public void equals_sameO_test() throws NoSuchAlgorithmException, IOException {
		MyFile mf1 = new MyFile();
		MyFile mf11 = mf1;
		File f = new File(workDir + tf2 + "/1.jpg");
		
		read(mf1, f);
		assertTrue(mf1.equals(mf11));
	}
	
	@Test
	public void equals_noInstance_test() throws NoSuchAlgorithmException, IOException {
		MyFile mf1 = new MyFile();
		File f = new File(workDir + tf2 + "/1.jpg");
		read(mf1, f);
		int noJpg = 0;
		
		assertFalse(mf1.equals(noJpg));
	}
	
	@Test
	public void equals_sameHash_test() throws NoSuchAlgorithmException, IOException {
	    File f1 = new File(workDir + tf2 + "/1.jpg");
	    File f1b = new File(workDir + tf2 + "/tf_22/1b.jpg");
		MyFile mf1 = new MyFile();
		MyFile mf1b = new MyFile();
		
		read(mf1, f1);
		read(mf1b, f1b);
		
		assertTrue(mf1.equals(mf1b));
	}
	
	@Test
	public void equals_notSame_test() throws NoSuchAlgorithmException, IOException {
		File f1 = new File(workDir + tf2 + "/1.jpg");
		File f2 = new File(workDir + tf2 + "/2.jpg");
	    MyFile mf1 = DI.getContext().getBean(IFileFactory.class).create();
		MyFile mf2 = new MyFile();
		
		read(mf1, f1);
		read(mf2, f2);
		
		assertFalse(mf1.equals(mf2));
	}
	
	private void read(MyFile mf, File f) throws FileNotFoundException, IOException{
	    mf.setAbsPath(f.getAbsolutePath());
	    mf.setDoc(f.lastModified());
	    mf.setHash(DigestUtils.md5Hex(new FileInputStream(f.getAbsolutePath())));
	    mf.setName(f.getName());
	    mf.setRelPath(f.getAbsolutePath());
	    
	    mf.getAbsPath();
	    mf.getDoc();
	    mf.getHash();
	    mf.getName();
	    mf.getRelPath();
	}
	
}
