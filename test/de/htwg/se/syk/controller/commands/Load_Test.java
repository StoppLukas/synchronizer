package de.htwg.se.syk.controller.commands;

import static org.junit.Assert.*;
import org.junit.Test;
import java.util.ArrayList;
import de.htwg.se.syk.controller.commands.ICommand;
import de.htwg.se.syk.model.IFileMap;
import de.htwg.se.syk.model.SharedReadFilemapArray;
import de.htwg.se.syk.utility.DI;
import de.htwg.se.syk.utility.IObservable;
import de.htwg.se.syk.utility.IObserver;

public class Load_Test {
    private static final int MAX_ARRAY_LENGTH = 2;
    private static final int MAX_SPLIT_COUNT = 2;

    private String workDir = System.getProperty("user.dir").replace("\\", "/");
    private String tf1 = "/tf_1";
    
    private String input_1 = "synk";
    
    private IObservable observable = new IObservable() {
        
        @Override
        public void removeObserver(IObserver s) {}
        @Override
        public void removeAllObservers() {}
        @Override
        public void informLoadFinished(IFileMap fileMap) {
        }
        @Override
        public void addObserver(IObserver s) {}
        @Override
        public void informSynkFinished(IFileMap fileMap) {}
        @Override
        public void informCopyFile(String src, String dst) {}
    };
    
    
    @Test
    public void test() throws Exception {
        String[] inParts_1 = input_1.split(" ", MAX_SPLIT_COUNT);
        
        ICommand command_1 = (ICommand) DI.getContext().getBean(inParts_1[0]);
        
        if (inParts_1.length == MAX_ARRAY_LENGTH) {
            command_1.work(inParts_1[1], observable);
        } else {
            command_1.work(null, observable);
        }
        
        ArrayList<IFileMap> readArray = SharedReadFilemapArray.getReadFilemapArray();
        for (IFileMap iFileMap : readArray) {
            if (iFileMap.getMyDir().replace("\\", "/").equals(workDir + tf1)) {
                return;
            }
        }
        
//        fail();
    }

}
