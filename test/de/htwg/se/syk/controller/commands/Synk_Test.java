package de.htwg.se.syk.controller.commands;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import de.htwg.se.syk.model.IFileMap;
import de.htwg.se.syk.model.SharedReadFilemapArray;
import de.htwg.se.syk.utility.DI;
import de.htwg.se.syk.utility.IObservable;
import de.htwg.se.syk.utility.IObserver;

public class Synk_Test {
    private static final int MAX_ARRAY_LENGTH = 2;
    private static final int MAX_SPLIT_COUNT = 2;
    
    String input = "synk";

    @Before
    public void before(){
        SharedReadFilemapArray.removeAll();
    }
    
    private IObservable observable = new IObservable() {
        
        @Override
        public void removeObserver(IObserver s) {}
        @Override
        public void removeAllObservers() {}
        @Override
        public void informLoadFinished(IFileMap fileMap) {
        }
        @Override
        public void addObserver(IObserver s) {}
        @Override
        public void informSynkFinished(IFileMap fileMap) {}
        @Override
        public void informCopyFile(String src, String dst) {}
    };
    
    @Test
    public void work_test() throws Exception {
        String[] inParts = input.split(" ", MAX_SPLIT_COUNT);
        
        ICommand command = (ICommand) DI.getContext().getBean(inParts[0]);
        
        if (inParts.length == MAX_ARRAY_LENGTH) {
            command.work(inParts[1], observable);
        } else {
            command.work(null, observable);
        }
        
        if(!SharedReadFilemapArray.getReadFilemapArray().isEmpty()){
            fail("The ReadFilemapArray in the Sharedclass is not Empty");
        }
        
    }

}
