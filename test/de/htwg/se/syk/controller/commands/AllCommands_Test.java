package de.htwg.se.syk.controller.commands;

import static org.junit.Assert.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import de.htwg.se.syk.utility.DI;
import de.htwg.se.syk.utility.IObservable;
import de.htwg.se.syk.utility.IObserver;
import de.htwg.se.syk.model.IFileMap;
import de.htwg.se.syk.model.SharedReadFilemapArray;

public class AllCommands_Test {
    private static final int MAX_ARRAY_LENGTH = 2;
    private static final int MAX_SPLIT_COUNT = 2;
    
    private String workDir = System.getProperty("user.dir").replace("\\", "/");
    private String tf1 = "/tf_1";
    private String tf2 = "/tf_2";
    
//    private String input1 = "load " + workDir + tf1;
//    private String input2 = "load " + workDir + tf2;
    private String input3 = "synk";
    private String input4 = "copy";
    
    private ArrayList<String> hl1 = new ArrayList<String>();
    private ArrayList<String> hl2 = new ArrayList<String>();
    
    private IObservable observable = new IObservable() {
        
        @Override
        public void removeObserver(IObserver s) {}
        @Override
        public void removeAllObservers() {}
        @Override
        public void informLoadFinished(IFileMap fileMap) {
        }
        @Override
        public void addObserver(IObserver s) {}
        @Override
        public void informSynkFinished(IFileMap fileMap) {}
        @Override
        public void informCopyFile(String src, String dst) {}
    };
    
    @Before
    public void before() {
        SharedReadFilemapArray.removeAll();
    }

    @Test
    public void test_allCommands() throws Exception {
        ArrayList<String[]> inList = new ArrayList<String[]>();
//        String[] in1 = input1.split(" ", MAX_SPLIT_COUNT);
//        inList.add(in1);
//        String[] in2 = input2.split(" ", MAX_SPLIT_COUNT);
//        inList.add(in2);
        String[] in3 = input3.split(" ", MAX_SPLIT_COUNT);
        inList.add(in3);
        String[] in4 = input4.split(" ", MAX_SPLIT_COUNT);
        inList.add(in4);
        
//        f�hre alle wokrs mit dazugeh�rigen argumenten aus.
        for (String[] comArg : inList) {
            ICommand command = (ICommand) DI.getContext().getBean(comArg[0]);
            
            if (comArg.length == MAX_ARRAY_LENGTH) {
                command.work(comArg[1], observable);
            } else {
                command.work(null, observable);
            }
        }
        
        loadDirIn_hashList(hl1, new File(workDir + tf1));
        loadDirIn_hashList(hl2, new File(workDir + tf2));
        
//        if(!hl1.containsAll(hl2)){
//            fail("hl1 has not all hashvelues from hl2");
//        }
//        if(!hl2.containsAll(hl1)){
//            fail("hl2 has not all hashvelues from hl1");
//        }
        
    }
    
    @After
    public void after() {
        SharedReadFilemapArray.removeAll();
        File folder = new File(workDir + tf1);
        removeAllFilesFromTestfolder(folder);
    }
    
    private void removeAllFilesFromTestfolder(File folder) {
        
        for (File file : folder.listFiles()) {
            if (!file.getName().equals("1.jpg")){
                FileUtils.deleteQuietly(file);
            }
        }
        
    }
    
    private void loadDirIn_hashList(ArrayList<String> hashList, File folder) throws FileNotFoundException, IOException {
        assertNotNull(folder);
        assertNotNull(hashList);
        for (File file : folder.listFiles()) {
            if (file.isDirectory()){
                loadDirIn_hashList(hashList, file);
            } else {
                FileInputStream fi = new FileInputStream(file);
                hashList.add(DigestUtils.md5Hex(fi));
                fi.close();
            }
        }
    }

}
