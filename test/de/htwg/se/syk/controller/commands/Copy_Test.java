package de.htwg.se.syk.controller.commands;

import static org.junit.Assert.*;
import java.io.File;
import org.junit.Before;
import org.junit.Test;
import de.htwg.se.syk.model.IFileMap;
import de.htwg.se.syk.model.SharedReadFilemapArray;
import de.htwg.se.syk.utility.DI;
import de.htwg.se.syk.utility.IObservable;
import de.htwg.se.syk.utility.IObserver;

public class Copy_Test {
    
    
    private static final int MAX_ARRAY_LENGTH = 2;
    private static final int MAX_SPLIT_COUNT = 2;
    
    private String workDir = System.getProperty("user.dir").replace("\\", "/");
    private String tf1 = "/tf_1";
    private String input = "copy";
    
    @Before
    public void before(){
        SharedReadFilemapArray.removeAll();
    }
    
    private IObservable observable = new IObservable() {
        
        @Override
        public void removeObserver(IObserver s) {}
        @Override
        public void removeAllObservers() {}
        @Override
        public void informLoadFinished(IFileMap fileMap) {
        }
        @Override
        public void addObserver(IObserver s) {}
        @Override
        public void informSynkFinished(IFileMap fileMap) {}
        @Override
        public void informCopyFile(String src, String dst) {}
    };
    
    @Test
    public void work_test() throws Exception {
        String dir1 = workDir + tf1;
        String[] inParts = input.split(" ", MAX_SPLIT_COUNT);
        
        ICommand command = (ICommand) DI.getContext().getBean(inParts[0]);
        
        if (inParts.length == MAX_ARRAY_LENGTH) {
            command.work(inParts[1], observable);
        } else {
            command.work(null, observable);
        }
        
        testDirBefore(new File(dir1));
        
    }
    
    private boolean testDirBefore(final File testFolder) {
        for (final File file : testFolder.listFiles()) {
            if (file.isDirectory()){
                testDirBefore(file);
            } else {
                if (!file.getName().equals("1.jpg")){
                    fail("not the expected file befor coppy!");
                }
            }
        }
        
        return true;
    }

}
