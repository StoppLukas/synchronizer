package de.htwg.se.syk.controller;

import static org.junit.Assert.*;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.NavigableSet;

import de.htwg.se.syk.aVIEW.aGUI.GUI;
import de.htwg.se.syk.aVIEW.aTUI.TUI;
import de.htwg.se.syk.controller.Controller;
import de.htwg.se.syk.model.SharedReadFilemapArray;
import de.htwg.se.syk.utility.DI;

public class Controller_Test {
	private static final Logger logger = LogManager.getLogger();
    private static final String workDir = System.getProperty("user.dir").replace("\\", "/");
    private static final String tf1 = "/tf_1";
    private static final String tf2 = "/tf_2";
    private static final String QUIT = "q";
    private static final String ONE_JPG = "1.jpg";
    private static final String LOAD1 = "load " + workDir + tf1;
    private static final String LOAD2 = "load " + workDir + tf2;
    private static final String SYNK = "synk";
    private static final String COPY = "copy";
    
    private Controller controller = null;
    private static TUI tui;
    
    @Before
    public void before() {
    	logger.info("Reinitialization of SharedReadFilemap Array");
    	SharedReadFilemapArray.removeAll();
    }
    
    @Test
    public void synk() throws NoSuchAlgorithmException, IOException, InterruptedException {
        assertTrue(SharedReadFilemapArray.getReadFilemapArray().isEmpty());
    	
        controller = (Controller) DI.getContext().getBean("Controller");
        controller.start();
//        controller.pushCommand(LOAD1);
//        controller.pushCommand(LOAD2);
        
        tui = (TUI) DI.getContext().getBean("TUI");
        tui.setController(controller);
        
        controller.addObserver(tui);
//        controller.removeObserver(tui);
//        controller.addObserver(tui);
//        controller.removeAllObservers();
        
        
        sleep(1000);
        
//        assertNotNull(SharedReadFilemapArray.getReadFilemapArray());
//        assertFalse(SharedReadFilemapArray.getReadFilemapArray().isEmpty());
//        NavigableSet<String> listOfTrackedKeys = SharedReadFilemapArray.getReadFilemapArray().get(0).getListOfTrackedKeys();
//        assertNotNull(listOfTrackedKeys);
//        
//        String fristKey = listOfTrackedKeys.first();
//        String firstFileName = SharedReadFilemapArray.getReadFilemapArray().get(0).getFilelistFromTrackedKey(fristKey).get(0).getName();
//        assertTrue((firstFileName.equalsIgnoreCase(ONE_JPG)));
        
        controller.pushCommand(SYNK);
        controller.pushCommand(COPY);
        controller.pushCommand("nix");
        controller.pushCommand(QUIT);
        controller.join();
    }
    
    @After
    public void after() {
        SharedReadFilemapArray.removeAll();
        File f = new File(workDir + tf1);
        removeAllFilesFromTestfolder(f);
    }

    private void sleep(long ms){
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    
private void removeAllFilesFromTestfolder(File folder) {
        
        for (File file : folder.listFiles()) {
            if (!file.getName().equals("1.jpg")){
                FileUtils.deleteQuietly(file);
            }
        }
    }
}
